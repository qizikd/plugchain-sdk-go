package bank

import (
	"gitee.com/qizikd/plugchain-sdk-go/codec"
	types2 "gitee.com/qizikd/plugchain-sdk-go/codec/types"
	cryptocodec "gitee.com/qizikd/plugchain-sdk-go/crypto/codec"
	"gitee.com/qizikd/plugchain-sdk-go/modules/auth"
	sdk "gitee.com/qizikd/plugchain-sdk-go/types"
)

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

func init() {
	cryptocodec.RegisterCrypto(amino)
	amino.Seal()
}

func RegisterInterfaces(registry types2.InterfaceRegistry) {
	registry.RegisterImplementations(
		(*sdk.Msg)(nil),
		&MsgSend{},
		&MsgMultiSend{},
	)

	registry.RegisterImplementations(
		(*auth.Account)(nil),
		&auth.BaseAccount{},
		&auth.EthAccount{},
	)
	//registry.RegisterImplementations(
	//	(*auth.EthAccount)(nil),
	//	&auth.EthAccount{},
	//)
}
