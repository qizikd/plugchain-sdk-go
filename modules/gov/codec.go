package gov

import (
	"gitee.com/qizikd/plugchain-sdk-go/codec"
	"gitee.com/qizikd/plugchain-sdk-go/codec/types"
	cryptocodec "gitee.com/qizikd/plugchain-sdk-go/crypto/codec"
	sdk "gitee.com/qizikd/plugchain-sdk-go/types"
)

var (
	amino     = codec.NewLegacyAmino()
	ModuleCdc = codec.NewAminoCodec(amino)
)

func init() {
	cryptocodec.RegisterCrypto(amino)
}

func RegisterInterfaces(registry types.InterfaceRegistry) {
	registry.RegisterImplementations((*sdk.Msg)(nil),
		&MsgSubmitProposal{},
		&MsgDeposit{},
		&MsgVote{},
	)
}
