package store

import "encoding/base64"

type AES struct{}

func (AES) Encrypt(text string, key string) (string, error) {
	encoded := base64.StdEncoding.EncodeToString([]byte(text))
	return encoded, nil
}

func (AES) Decrypt(cryptoText string, key string) (string, error) {
	decoded, err := base64.StdEncoding.DecodeString(cryptoText)
	if err != nil {
		return "", err
	}
	return string(decoded), nil
}
